class Code
  attr_reader :pegs

  PEGS = {
    'r' => 'red',
    'g' => 'green',
    'b' => 'blue',
    'y' => 'yellow',
    'o' => 'orange',
    'p' => 'purple'
  }

  def self.random
    new(Array.new(4) { PEGS.keys.sample })
  end

  def self.parse(seq)
    seq.downcase!
    raise unless seq.chars.uniq.all? { |char| PEGS.include?(char) }
    new(seq.chars)
  end

  def initialize(pegs)
    @pegs = pegs
  end

  def [](index)
    @pegs[index]
  end

  def exact_matches(other)
    @pegs.each_index.reduce(0) do |count, index|
      @pegs[index] == other[index] ? count += 1 : count
    end
  end

  def near_matches(other)
    (@pegs & other.pegs).count - exact_matches(other)
  end

  def ==(other)
    return false unless other.is_a?(self.class)
    exact_matches(other) == 4
  end
end

class Game
  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  def get_guess
    print "What's your guess?: "
    Code.parse(gets.chomp)
  end

  def display_matches(code)
    puts "exact matches: #{code.exact_matches(secret_code)}"
    puts "near matches: #{code.near_matches(secret_code)}"
  end
end
